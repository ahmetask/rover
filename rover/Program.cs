﻿using System;
using rover.Controller;
using rover.Model;

namespace rover
{
    internal class Program
    {
        private static Terrain _terrain;
        private static Robot _robot;
        static void Main(string[] args)
        {

            _terrain = GetTerrain();
            SetRobot();
            
          
            var view = new View.View();
            var robotController = new RobotController(_terrain ,_robot ,view);
      

            robotController.StartGame();
            

        }
        public static void SetRobot()
        {
            string[] sizes;
            var x = 0;
            var y = 0;

            bool isNumericX ;
            bool isNumericY ;
            bool isValidPosition ;
            do
            {
                isNumericX = false;
                isNumericY = false;
                isValidPosition = false;
                Console.WriteLine("Enter robot x and y(# # N):");
                var sizeString = Console.ReadLine();

                sizes = sizeString.Split(' ');
                if (sizes.Length == 3)
                {
                    isNumericX = int.TryParse(sizes[0], out x);
                    isNumericY = int.TryParse(sizes[1], out y);
                    sizes[2] = sizes[2].ToUpper();
                    if (sizes[2] == "N" || sizes[2] == "E" || sizes[2] == "W" || sizes[2] == "S")
                        isValidPosition = true;

                }
            } while ( x < 0 || y < 0 || x>=_terrain.X || y>=_terrain.Y|| !isNumericX || !isNumericY || !isValidPosition);

            var geo = 0 ;
            switch (sizes[2])
            {
                case "N":
                    geo = 0;
                    break;
                case "E":
                    geo = 1;
                    break;
                case "S":
                    geo = 2;
                    break;
                case "W":
                    geo = 3;
                    break;
            }

            _robot  = new Robot(x,y ,geo);
            _terrain.Nodes[x,y].Val = sizes[2];
        }
        public static Terrain GetTerrain()
        {
            var x = 0;
            var y = 0;
            var isNumericX = false;
            var isNumericY = false;
            do
            {
                Console.WriteLine("Enter terrain x and y(# #):");
                var sizeString = Console.ReadLine();

                var sizes = sizeString.Split(' ');
                if (sizes.Length == 2)
                {
                    isNumericX = int.TryParse(sizes[0], out x);
                    isNumericY = int.TryParse(sizes[1], out y);
                }
            } while ( x == 0 || y == 0 || !isNumericX || !isNumericY);


            return  new Terrain(x+1, y+1);
        }
    }
}