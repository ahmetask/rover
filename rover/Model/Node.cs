﻿namespace rover.Model
{
    public class Node
    {
        public int X {get; set; }
        public int Y { get; set; }
        public bool IsEmpty { get; set; }
        

        public string Val { get; set; }

        public Node(int x, int y)
        {
            X = x;
            Y = y;
            IsEmpty = true;
            Val = ".";
        }

    }
}
