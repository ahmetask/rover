﻿using System;

namespace rover.Model
{
    public class Terrain
    {
        public int X { get; }
        public int Y { get; }
        public  Node[,] Nodes{ get; set; } 
        public Terrain(int x , int y)
        {
            Nodes =new Node[x,y];
            SetTerrainNodes();
            X = x;
            Y = y;
        }

        public void SetTerrainNodes()
        {
            for (int i = 0; i < Nodes.GetLength(0); i++)
            {
                for (int j = 0; j < Nodes.GetLength(1); j++)
                {

                    Nodes[i, j] = new Node(i, j);

                }
                Console.WriteLine();
            }
        }
    }
}
