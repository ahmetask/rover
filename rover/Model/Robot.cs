﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rover.Model
{
    public class Robot
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int FacePosition { get; set; }


        public Robot(int x, int y, int facePosition)
        {
            X = x;
            Y = y;
            FacePosition = facePosition;
        }

    }
}
