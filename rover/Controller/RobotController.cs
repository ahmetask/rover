﻿using System;
using System.Collections.Generic;
using rover.Model;

namespace rover.Controller
{
    public class RobotController
    {
        public  Terrain _terrain;
        public  List<Robot> _robot = new List<Robot>();
        public int _robotindex;
        public  View.View _view;
        public List<string> _commandhistory = new List<string>();
        public RobotController(Terrain terrain, Robot robot, View.View view)
        {
            _terrain = terrain;          
            _robot.Add(robot); 
            terrain.Nodes[robot.X, robot.Y].IsEmpty = false;
            _view = view;
        }


        public void StartGame()
        {    
            while (true)
            {
                var console = Console.ReadLine();
                if (console == "e")
                    break;

                var commandlist = console.Split(' ');

                if (commandlist.Length == 3)
                {

                    var x = 0;
                    var y = 0;
                    var isNumericX = false;
                    var isNumericY = false;
                    var isValidPosition = false;


                    if (commandlist.Length == 3)
                    {
                        isNumericX = int.TryParse(commandlist[0], out x);
                        isNumericY = int.TryParse(commandlist[1], out y);
                        commandlist[2] = commandlist[2].ToUpper();
                        if (commandlist[2] == "N" || commandlist[2] == "E" || commandlist[2] == "W" ||
                            commandlist[2] == "S")
                            isValidPosition = true;
                    }
                    if (isNumericX && isNumericY && isValidPosition)
                    {
                        _robotindex++;
                        int geo = 0;
                        switch (commandlist[2])
                        {
                            case "N":
                                geo = 0;
                                break;
                            case "E":
                                geo = 1;
                                break;
                            case "S":
                                geo = 2;
                                break;
                            case "W":
                                geo = 3;
                                break;
                        }
                        ChangeRobot(x, y, geo);
                    }
                    else
                    {
                        Console.WriteLine("Wrong input");
                    }

                }
                else
                {

                   
                    console = console.ToUpper();
                    
               
                     
                    _commandhistory.Add(console);
          

                    if (_robotindex == 1 && _commandhistory.Count == 2)
                    {
                        ApplyCommand();
                        foreach (var rbt in _robot)
                        {
                            _view.PrintRobot(rbt);
                        }

                        _commandhistory.Clear();
                        _robot.Clear();
                        _robotindex = 0;
                        Console.WriteLine("Exit : 'e'");
                        if (Console.ReadLine().Equals("e"))
                            break;
                    }
                }



            }
        


        }

        public void ApplyCommand()
        {
            for(var i = 0 ; i< _commandhistory.Count; i++) {
         
                var command = _commandhistory[i].ToCharArray();
                _robotindex = i;
                foreach (var val in command)
                {
                    int ascii = val;
                    if (ascii == 76 || ascii == 82 || ascii == 77)
                    {

                        if (ascii == 77)
                        {
                            MoveForward();
                     
                        }
                           
                        else
                        {
                            Rotate(val);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Contains wrong input");
                       
                        
                    }
                 
                }
           
            }
        }
        public void ChangeRobot(int x , int y ,int geo)
        {
            if (x < 0 || x >= _terrain.X || y < 0 || y >= _terrain.Y)
            {
                Console.WriteLine("Enter robot's x and y in terrain");
                return;
            }              
            var rbt= new Robot(x, y, geo);        
            _robot.Add(rbt);
           
        }
       

        public bool MoveForward()
        {

            var canmove = false;
            if (_robot[_robotindex] != null)
            {
                switch (_robot[_robotindex].FacePosition)
                {
                    case 3:
                        if (_robot[_robotindex].X > 0)
                        {


                            _robot[_robotindex].X = _robot[_robotindex].X - 1;

                            canmove = true;


                        }
                        break;
                    case 1:
                        if (_robot[_robotindex].X < _terrain.X - 1)
                        {

                            _robot[_robotindex].X = _robot[_robotindex].X + 1;
                            canmove = true;


                        }
                        break;
                    case 2:
                        if (_robot[_robotindex].Y > 0)
                        {


                            _robot[_robotindex].Y -= 1;

                            canmove = true;

                        }
                        break;
                    case 0:
                        if (_robot[_robotindex].Y < _terrain.Y - 1)
                        {

                            _robot[_robotindex].Y += 1;

                            canmove = true;

                        }
                        break;
                }
                if (canmove)
                {

                    return true;
                }
                else
                {


                    Console.WriteLine("Robot Falls");
                    Console.ReadKey();
                    return false;
                }
            }

            return false;


        }


        public void Rotate(char rot)
        {
            if (_robot[_robotindex] != null)
            {
                if (rot == 'L')
                {
                    if (_robot[_robotindex].FacePosition == 0)
                        _robot[_robotindex].FacePosition = 3;
                    else
                        _robot[_robotindex].FacePosition -= 1;
                }
                else if (rot == 'R')
                {
                    if (_robot[_robotindex].FacePosition == 3)
                        _robot[_robotindex].FacePosition = 0;
                    else
                        _robot[_robotindex].FacePosition += 1;
                }
            }
        
        }
        
    }
}
