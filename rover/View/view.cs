﻿using System;
using rover.Model;

namespace rover.View
{
    public class View
    {

        public void PrintRobot(Robot robot)
        {
            string facePosition = "N";
            switch (robot.FacePosition)
            {
                case 0 :
                    facePosition = "N";
                    break;
                case 1:
                    facePosition = "E";
                    break;
                case 2:
                    facePosition = "S";
                    break;
                case 3:
                    facePosition = "W";
                    break;
            }
            Console.WriteLine(robot.X + " " + robot.Y + " " + facePosition);
        }
  
    }
}
