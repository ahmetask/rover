using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using rover.Controller;
using rover.Model;
using rover.View;

namespace RoverUnitTest
{
    [TestClass]
    public class RoverControllerTest
    {


        public  Terrain _terrain;
        public  List<Robot> _robot = new List<Robot>();
  
        public  rover.View.View _view;
        private  List<string> _commandhistory = new List<string>();
    

    

        [TestInitialize]
        public void Initialize()
        {
            _terrain = new Terrain(5,5);
            _robot = new List<Robot> { new Robot(0, 0, 1)};




            _view  = new View();

            string[] cmd = new[] { "LMLMLMLMM", "MMRMMRMRRM" };
            
            _commandhistory.Add(cmd[0]);
            _commandhistory.Add(cmd[1]);

          
                    
                
                

        }

        [TestMethod]
        public void MoveForward ()
        {
            RobotController robotController = new RobotController(_terrain, new Robot(1, 2, 0), _view);



            robotController._robot.Add(new Robot(3, 3, 1));
            robotController._terrain = _terrain;
            robotController._view = _view;
            robotController._commandhistory = _commandhistory;
            robotController._robotindex = 0;
            robotController.ApplyCommand();
            Assert.AreEqual(robotController._robot[0].X, 1);
            Assert.AreEqual(robotController._robot[0].Y, 3);
            Assert.AreEqual(robotController._robot[0].FacePosition,0);
            Assert.AreEqual(robotController._robot[1].X, 5);
            Assert.AreEqual(robotController._robot[1].Y, 1);
            Assert.AreEqual(robotController._robot[1].FacePosition, 1);


        }
    }
}
